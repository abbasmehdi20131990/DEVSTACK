#!bin/bash/
#install the necessary dependencies 
sudo apt-get install pkg-config libxml2-dev libxmlsec1-dev libxmlsec1-openssl -y
sudo apt-get update 
sudo apt -y install python3-pip python3-pyscard -y
pip3 install PyOpenSSL 
pip3 install yubikey-manager 
sudo apt install git -y
git clone https://github.com/mehcode/python-xmlsec.git
cd python-xmlsec
pip install .
sudo apt-get install -y sshpass
echo "$USER ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/$USER
sudo apt update
sudo apt dist-upgrade --yes
sudo useradd -s /bin/bash -d /opt/stack -m stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
sudo -u stack -i git clone https://opendev.org/openstack/devstack
sudo -u stack -i cd devstack
sudo -u stack -i cp samples/local.conf .
sudo -u stack -i tee /opt/stack/devstack/local.conf<<EOF
[[local|localrc]]
RECLONE=False
HOST_IP=192.168.56.107

DATABASE_PASSWORD=password
ADMIN_PASSWORD=password
SERVICE_PASSWORD=password
SERVICE_TOKEN=password
RABBIT_PASSWORD=password

USE_PYTHON3=True
IPV4_ADDRS_SAFE_TO_USE=192.168.56.0/24
FIXED_RANGE=192.168.56.0/24
NETWORK_GATEWAY=192.168.56.1
FLOATING_RANGE=172.30.5.0/24
PUBLIC_NETWORK_GATEWAY=172.30.5.1

# Pre-requisites
#ENABLED_SERVICES=rabbit,mysql,key
##########
# Manila #
##########
#enable_plugin manila https://opendev.org/openstack/manila
#enable_plugin manila-ui https://opendev.org/openstack/manila-ui

##########
# Heat   #
##########
enable_plugin heat https://git.openstack.org/openstack/heat
enable_plugin heat-dashboard https://opendev.org/openstack/heat-dashboard
enable_service h-eng h-api h-api-cfn h-api-cw

# ##########
# # Trove  #
# ##########
#enable_plugin trove https://opendev.org/openstack/trove
#enable_plugin trove-dashboard https://opendev.org/openstack/trove-dashboard

#enable_plugin octavia https://opendev.org/openstack/octavia
#enable_plugin octavia-dashboard https://opendev.org/openstack/octavia-dashboard

#enable_plugin neutron-lbaas https://git.openstack.org/openstack/neutron-lbaas
#enable_plugin octavia https://git.openstack.org/openstack/octavia.git


# enable_plugin neutron-lbaas-dashboard https://opendev.org/openstack/neutron-lbaas-dashboard

# ############
# # Barbican #
# ############
#enable_plugin barbican https://opendev.org/openstack/barbican
#enable_plugin barbican_ui https://github.com/openstack/barbican-ui


# # Nova
# enable_service n-api
# enable_service n-cpu
# enable_service n-cond
# enable_service n-sch
# enable_service n-api-meta
# enable_service placement-api
# enable_service placement-client

# # Glance
# enable_service g-api
# enable_service g-reg

# # Cinder
# enable_service cinder
# enable_service c-api
# enable_service c-vol
# enable_service c-sch


# Swift
#ENABLED_SERVICES+=,swift
#SWIFT_HASH=66a3d6b56c1f479c8b4e70ab5c2000f5
#SWIFT_REPLICAS=1

#ENABLED_SERVICES+=,octavia,o-api,o-cw,o-hk,o-hm,o-da
#ENABLED_SERVICES+=,q-lbaasv2,octavia,o-cw,o-hk,o-hm,o-api

#enable_plugin monasca-agent https://github.com/openstack/monasca-agent

EOF
sudo -u stack -i /opt/stack/devstack/./stack.sh
openstack image create ubuntu --file bionic-server-cloudimg-amd64.img --public --tag ubuntu --min-ram 1024 --disk-format qcow2 --min-disk 10
openstack image list
###### Creation du Gabarit ds1024M du script projet 
openstack flavor create ds1024M --ram 1024 --disk 10 --vcpus 1


